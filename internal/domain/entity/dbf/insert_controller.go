package dbf

import (
	"fmt"
	"github.com/Valentin-Kaiser/go-dbase/dbase"
	"sgbd/internal/domain/entity/statements"
)

func InsertValues(insert *statements.InsertStatement) error {
	if !tableExists(insert.TableName) {
		return fmt.Errorf(
			"table-name '%s' doesn't exists", insert.TableName,
		)
	}
	table, err := dbase.OpenTable(
		&dbase.Config{
			Filename:   DATABASES_PATH + "/" + insert.TableName + DBF_EXTNSION,
			TrimSpaces: true,
			WriteLock:  true,
		},
	)
	if err != nil {
		return dbase.GetErrorTrace(err)
	}
	defer table.Close()

	row := table.NewRow()
	if err != nil {
		return dbase.GetErrorTrace(err)
	}
	for k, v := range insert.ToInsert {
		err = row.FieldByName(k).SetValue(v)
		if err != nil {
			return err
		}
	}
	err = row.Write()
	if err != nil {
		return dbase.GetErrorTrace(err)
	}
	return nil
}
