package dbf

import (
	"fmt"
	"golang.org/x/text/encoding/charmap"
	"sgbd/internal/domain/entity/statements"

	"github.com/Valentin-Kaiser/go-dbase/dbase"
)

func CreateTable(create *statements.CreateStatement) error {
	if tableExists(create.TableName) {
		return fmt.Errorf(
			"table-name '%s' just exists", create.TableName,
		)
	}
	file, err := dbase.NewTable(
		dbase.FoxProVar,
		&dbase.Config{
			Filename:   create.TableName + ".dbf",
			Converter:  dbase.NewDefaultConverter(charmap.Windows1250),
			TrimSpaces: true,
		},
		create.Columns,
		64,
		nil,
	)
	if err != nil {
		return err
	}
	defer file.Close()

	err = moveFile(create.TableName+DBF_EXTNSION, getDatabasePath()+"/"+create.TableName+DBF_EXTNSION)
	if err != nil {
		return err
	}
	return nil
}

func Teste() {}
