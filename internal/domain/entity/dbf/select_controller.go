package dbf

import (
	"fmt"
	"github.com/Valentin-Kaiser/go-dbase/dbase"
	"golang.org/x/text/encoding/charmap"
	"sgbd/internal/domain/entity/extensions"
	"sgbd/internal/domain/entity/statements"
)

func SelectValues(selectStatement *statements.SelectStatement) (*extensions.Table, error) {
	if !tableExists(selectStatement.TableName) {
		return nil, fmt.Errorf(
			"table-name '%s' doesn't exists", selectStatement.TableName,
		)
	}
	table, err := dbase.OpenTable(
		&dbase.Config{
			Filename:   DATABASES_PATH + "/" + selectStatement.TableName + DBF_EXTNSION,
			Converter:  dbase.NewDefaultConverter(charmap.Windows1250),
			TrimSpaces: true,
		},
	)
	if err != nil {
		return nil, dbase.GetErrorTrace(err)
	}
	return &extensions.Table{
		Table: table,
	}, nil
}
