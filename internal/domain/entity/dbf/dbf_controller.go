package dbf

import (
	"fmt"
	"os"
	"path/filepath"
)

const (
	DATABASES_PATH = "internal/databases"
	DBF_EXTNSION   = ".DBF"
)

func getDatabasePath() string {
	path, _ := os.Getwd()
	finalPath := filepath.Join(path, DATABASES_PATH)
	return finalPath
}
func getPath() string {
	path, _ := os.Getwd()
	return path
}
func tableExists(tableName string) bool {
	_, err := os.Stat(filepath.Join(getDatabasePath(), "/"+tableName+DBF_EXTNSION))
	return !os.IsNotExist(err)
}

func moveFile(sourcePath, destinationPath string) error {
	err := os.Rename(sourcePath, destinationPath)
	if err != nil {
		return fmt.Errorf("error moving file: %v", err)
	}
	return nil
}
