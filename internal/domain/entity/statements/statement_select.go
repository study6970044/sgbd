package statements

import (
	"sgbd/internal/domain/entity/types"
)

type SelectStatement struct {
	TableName string
	Columns   []string
}

func NewSelectStatement(strQueryStringSlice []string) (*SelectStatement, error) {
	columns := make([]string, 0)
	for i := 1; i < len(strQueryStringSlice); i++ {
		if strQueryStringSlice[i] == "," {
			continue
		} else if strQueryStringSlice[i] == types.FROM_KEYWORD {
			break
		}
		columns = append(columns, strQueryStringSlice[i])
	}
	tableName := strQueryStringSlice[len(strQueryStringSlice)-2]
	return &SelectStatement{
		TableName: tableName,
		Columns:   columns,
	}, nil
}
