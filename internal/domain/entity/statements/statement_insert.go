package statements

import (
	"sgbd/internal/domain/entity/types"
)

type InsertStatement struct {
	TableName string
	ToInsert  map[string]string
}

func NewInsertStatement(strQueryStringSlice []string) (*InsertStatement, error) {
	toInsert := make(map[string]string)

	var valuesKeywordIndex int
	for i := 4; i < len(strQueryStringSlice)-1; i += 1 {
		if strQueryStringSlice[i] == types.VALUES_KEYWORD {
			valuesKeywordIndex = i
			break
		}
	}
	j := 2 + valuesKeywordIndex
	for i := 4; i < len(strQueryStringSlice)-1; i += 1 {
		if strQueryStringSlice[i] == "," {
			j += 1
			continue
		} else if i == valuesKeywordIndex-1 {
			break
		}
		toInsert[strQueryStringSlice[i]] = strQueryStringSlice[j]
		j += 1
	}
	tableName := strQueryStringSlice[2]
	return &InsertStatement{
		TableName: tableName,
		ToInsert:  toInsert,
	}, nil
}
