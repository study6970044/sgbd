package statements

import (
	"github.com/Valentin-Kaiser/go-dbase/dbase"
	"sgbd/internal/domain/entity/types"
	"strings"
)

type CreateStatement struct {
	TableName string
	Columns   []*dbase.Column
}

func NewCreateStatement(strQueryStringSlice []string) (*CreateStatement, error) {
	columnsToCreate := make([]*dbase.Column, 0)
	tableName := strings.ToUpper(strQueryStringSlice[2])
	var colDbaseDatatype dbase.DataType
	var colLength uint8
	var colDecimals uint8
	for i := 4; i < len(strQueryStringSlice)-1; i += 3 {
		colName := strQueryStringSlice[i]
		colDataType := strQueryStringSlice[i+1]
		switch colDataType {
		case types.INT_KEYWORD:
			colDbaseDatatype = dbase.Integer
			colLength = types.INT_STD_SIZE
		case types.FLOAT_KEYWORD:
			colDbaseDatatype = dbase.Float
			colLength = types.FLOAT_STD_SIZE
			colDecimals = types.FLOAT_STD_PRECISION
		case types.STRING_KEYWORD:
			colDbaseDatatype = dbase.Character
			colLength = types.STRING_STD_SIZE
		case types.TRUEBOOLEAN_KEYWORD, types.FALSEBOOLEAN_KEYWORD:
			colDbaseDatatype = dbase.Logical
			colLength = 8
		}
		col, err := dbase.NewColumn(
			colName, colDbaseDatatype, colLength,
			colDecimals, false)

		if err != nil {
			return nil, err
		}
		columnsToCreate = append(columnsToCreate, col)
	}
	return &CreateStatement{
		TableName: tableName,
		Columns:   columnsToCreate,
	}, nil
}
