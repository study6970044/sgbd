package parsers

import "fmt"

/*
INSERT INTO table (col1, col2) VALUES (val1, val2);
*/
func parserInsert(strQueryStringSlice []string) error {
	if strQueryStringSlice[1] != "INTO" {
		return fmt.Errorf(
			"expected 'INTO' keyword but received '%s'", strQueryStringSlice[1],
		)
	}
	if err := isValidTableName(strQueryStringSlice[2]); err != nil {
		return err
	}
	if strQueryStringSlice[3] != "(" {
		return fmt.Errorf(
			"expected '(' punctuantion but received '%s'", strQueryStringSlice[3],
		)
	}
	if strQueryStringSlice[len(strQueryStringSlice)-1] != ";" {
		return fmt.Errorf(
			"expected ';' keyword but received '%s'", strQueryStringSlice[len(strQueryStringSlice)-1],
		)
	}
	if strQueryStringSlice[len(strQueryStringSlice)-2] != ")" {
		return fmt.Errorf(
			"expected ')' keyword but received '%s'", strQueryStringSlice[len(strQueryStringSlice)-2],
		)
	}

	var valuesKeywordIndex int
	mustBeAColumn := true
	for i := 4; i < len(strQueryStringSlice)-1; i += 1 {
		if err := isValidColumnName(strQueryStringSlice[i]); err != nil &&
			mustBeAColumn {
			return err
		} else if err := isValidColumnName(strQueryStringSlice[i]); err == nil &&
			mustBeAColumn {
			mustBeAColumn = false
		} else if isDQLCommandKeyword(strQueryStringSlice[i]) ||
			strQueryStringSlice[i+1] == "(" {
			return fmt.Errorf(
				"expected ')' punctuation but received '%s'", strQueryStringSlice[i],
			)
		} else if strQueryStringSlice[i] == ")" {
			valuesKeywordIndex = i + 1
			break
		} else if err := isValidColumnName(strQueryStringSlice[i]); err == nil &&
			strQueryStringSlice[i] != "," && !mustBeAColumn {
			return fmt.Errorf(
				"expected ',' punctuation but received '%s'", strQueryStringSlice[i],
			)
		} else if strQueryStringSlice[i] == "," && !mustBeAColumn {
			mustBeAColumn = true
		}
	}

	if strQueryStringSlice[valuesKeywordIndex] != "VALUES" {
		return fmt.Errorf(
			"expected 'VALUES' keyword but received '%s'", strQueryStringSlice[valuesKeywordIndex],
		)
	}
	if strQueryStringSlice[valuesKeywordIndex+1] != "(" {
		return fmt.Errorf(
			"expected '(' punctuation but received '%s'", strQueryStringSlice[valuesKeywordIndex+1],
		)
	}
	mustBeAColumn = true
	for i := valuesKeywordIndex + 2; i < len(strQueryStringSlice)-1; i += 1 {
		if err := isValidColumnName(strQueryStringSlice[i]); err != nil &&
			mustBeAColumn {
			return err
		} else if err := isValidColumnName(strQueryStringSlice[i]); err == nil &&
			mustBeAColumn {
			mustBeAColumn = false
		} else if isDQLCommandKeyword(strQueryStringSlice[i]) ||
			strQueryStringSlice[i+1] == "(" {
			return fmt.Errorf(
				"expected ')' punctuation but received '%s'", strQueryStringSlice[i],
			)
		} else if strQueryStringSlice[i] == ")" {
			break
		} else if err := isValidColumnName(strQueryStringSlice[i]); err == nil &&
			strQueryStringSlice[i] != "," && !mustBeAColumn {
			return fmt.Errorf(
				"expected ',' punctuation but received '%s'", strQueryStringSlice[i],
			)
		} else if strQueryStringSlice[i] == "," && !mustBeAColumn {
			mustBeAColumn = true
		}
	}

	return nil
}
