package parsers

import "fmt"

/* Syntax supported
SELECT col1, ..., colN FROM table_name;
*/

func parserSelect(strQueryStringSlice []string) error {
	if strQueryStringSlice[len(strQueryStringSlice)-1] != ";" {
		return fmt.Errorf(
			"expected ';' keyword but received '%s'", strQueryStringSlice[len(strQueryStringSlice)-1],
		)
	} else if err := isValidTableName(strQueryStringSlice[len(strQueryStringSlice)-2]); err != nil {
		return err
	} else if strQueryStringSlice[len(strQueryStringSlice)-3] != "FROM" {
		return fmt.Errorf("expected 'FROM' keyword but received '%s'", strQueryStringSlice[len(strQueryStringSlice)-3])
	}

	mustBeAColumn := true
	for i, _ := range strQueryStringSlice {
		if i == 0 || i == len(strQueryStringSlice)-1 ||
			i == len(strQueryStringSlice)-2 || i == len(strQueryStringSlice)-3 {
			continue
		}
		err := isValidTableName(strQueryStringSlice[i+1])
		if mustBeAColumn {
			if err := isValidTableName(strQueryStringSlice[i]); err != nil {
				return err
			}
			mustBeAColumn = false
		} else if strQueryStringSlice[i] == "," && err != nil {
			return err
		} else if strQueryStringSlice[i] == "," {
			mustBeAColumn = true
		} else if mustBeAColumn && strQueryStringSlice[i] != "," {
			return fmt.Errorf(
				"expected ',' keyword but received '%s'", strQueryStringSlice[i])
		} else if !mustBeAColumn && err != nil {
			return fmt.Errorf(
				"expected ',' keyword but received '%s'", strQueryStringSlice[i])
		}

	}
	return nil
}
