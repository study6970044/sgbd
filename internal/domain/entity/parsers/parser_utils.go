package parsers

import (
	"fmt"
	"sgbd/internal/domain/entity/dbf"
	"sgbd/internal/domain/entity/statements"
	"sgbd/internal/domain/entity/types"
	"strings"
)

func ParserString(strUnknown string) (interface{}, error) {
	queryStringSlice := NormalizeQueryString(strUnknown)
	var err error
	var res interface{}
	switch strings.ToUpper(queryStringSlice[0]) {
	case "CREATE":
		err = parserCreate(queryStringSlice)
		if err != nil {
			return nil, err
		}
		createStatement, err := statements.NewCreateStatement(queryStringSlice)
		if err != nil {
			return nil, err
		}
		err = dbf.CreateTable(createStatement)
		if err != nil {
			return nil, err
		}
	case "SELECT":
		err = parserSelect(queryStringSlice)
		if err != err {
			return nil, err
		}
		selectStatement, err := statements.NewSelectStatement(queryStringSlice)
		if err != nil {
			return nil, err
		}
		res, err := dbf.SelectValues(selectStatement)
		if err != nil {
			return nil, err
		}
		return res, err
	case "INSERT":
		err = parserInsert(queryStringSlice)
		if err != nil {
			return nil, err
		}
		insertStatement, err := statements.NewInsertStatement(queryStringSlice)
		if err != nil {
			return nil, err
		}
		err = dbf.InsertValues(insertStatement)
		if err != nil {
			return nil, err
		}
	default:
		err = fmt.Errorf("'%s' not identified as a valid command", strings.ToUpper(queryStringSlice[0]))
	}
	return res, err
}

func NormalizeQueryString(strToParser string) []string {
	strToParser = strings.ToUpper(strToParser)
	var tokensStrParsed []string
	var tokensStrParsedFinal []string
	var lastStr string
	var postStr string
	var actStr string
	for i, str := range strToParser {
		actStr = string(str)
		if i == 0 {
			tokensStrParsed = append(tokensStrParsed, actStr)
			continue
		} else if i == len(strToParser)-1 {
			if isPunctuationKeyword(actStr) {
				tokensStrParsed = append(tokensStrParsed, " ")
				tokensStrParsed = append(tokensStrParsed, postStr)
			} else {
				tokensStrParsed = append(tokensStrParsed, actStr)
			}
			continue
		}

		lastStr = string(strToParser[i-1])
		postStr = string(strToParser[i+1])
		if isPunctuationKeyword(actStr) {
			if lastStr != " " {
				tokensStrParsed = append(tokensStrParsed, " ")
			}
			tokensStrParsed = append(tokensStrParsed, actStr)
			if postStr != " " {
				tokensStrParsed = append(tokensStrParsed, " ")
			}
		} else if isPunctuationKeyword(actStr) && isPunctuationKeyword(postStr) {
			if actStr != " " {
				tokensStrParsed = append(tokensStrParsed, " ")
			}
			tokensStrParsed = append(tokensStrParsed, postStr)
		} else {
			tokensStrParsed = append(tokensStrParsed, actStr)
		}
	}
	var strBuilder string
	for i, actStr := range tokensStrParsed {
		if i == len(tokensStrParsed)-1 {
			tokensStrParsedFinal = append(tokensStrParsedFinal, strBuilder+actStr)
		}
		if actStr == " " && strBuilder != "" {
			tokensStrParsedFinal = append(tokensStrParsedFinal, strBuilder)
			strBuilder = ""
		} else {
			strBuilder += strings.TrimSpace(actStr)
		}
	}
	return tokensStrParsedFinal
}

func isDQLCommandKeyword(token string) bool {
	for _, val := range types.DqlComands {
		if val == token {
			return true
		}
	}
	return false
}
func isPunctuationKeyword(token string) bool {
	for _, val := range types.PunctiationKeyword {
		if val == token {
			return true
		}
	}
	return false
}
func isDataTypeKeyword(token string) bool {
	for _, val := range types.DataTypeKeywords {
		if val == token {
			return true
		}
	}
	return false
}

func isValidColumnName(columnName string) error {
	if isDataTypeKeyword(columnName) {
		return fmt.Errorf(
			"expected a valid column-name but received a Data-Type '%s'", columnName,
		)
	} else if isDQLCommandKeyword(columnName) {
		return fmt.Errorf(
			"expected a valid column name but received a DQL command %s", columnName,
		)
	} else if isPunctuationKeyword(columnName) {
		return fmt.Errorf(
			"expected a valid column-name but received a Punctuntion command '%s'", columnName,
		)
	}
	for _, char := range columnName {
		char := string(char)
		if isPunctuationKeyword(char) {
			return fmt.Errorf(
				"columns must not have Punctiantio characterns in her body "+
					"column '%s' have '%s'", columnName, char,
			)
		}
	}
	return nil
}
func isValidTableName(tableName string) error {
	if isDataTypeKeyword(tableName) {
		return fmt.Errorf(
			"expected a valid table-name but received a Data-Type '%s'", tableName,
		)
	} else if isDQLCommandKeyword(tableName) {
		return fmt.Errorf(
			"expected a valid table-name but received a DQL command '%s'", tableName,
		)
	} else if isPunctuationKeyword(tableName) {
		return fmt.Errorf(
			"expected a valid table-name but received a Punctuntion command '%s'", tableName,
		)
	}
	for _, char := range tableName {
		char := string(char)
		if isPunctuationKeyword(char) {
			return fmt.Errorf(
				"table-names must not have Punctiantion characters in her body "+
					"table-name '%s' have '%s'", tableName, char,
			)
		}
	}
	return nil
}
