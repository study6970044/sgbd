package parsers

import "fmt"

/* Syntax supported
CREATE TABLE table_name (
	column1 datatype,
	column2 datatype,
	column3 datatype,
....
);
*/

func parserCreate(strQueryStringSlice []string) error {
	if strQueryStringSlice[1] != "TABLE" {
		return fmt.Errorf(
			"expected 'TABLE' keyword but received '%s'", strQueryStringSlice[1],
		)
	}
	if err := isValidTableName(strQueryStringSlice[2]); err != nil {
		return err
	}
	if strQueryStringSlice[3] != "(" {
		return fmt.Errorf(
			"expected '(' but received '%s'", strQueryStringSlice[3],
		)
	}

	if strQueryStringSlice[len(strQueryStringSlice)-1] != ";" {
		return fmt.Errorf(
			"expected ';' but received '%s'", strQueryStringSlice[len(strQueryStringSlice)-1],
		)
	}
	if strQueryStringSlice[len(strQueryStringSlice)-2] != ")" {
		return fmt.Errorf(
			"expected ')' but received '%s'", strQueryStringSlice[len(strQueryStringSlice)-2],
		)
	}
	for i := 4; i < len(strQueryStringSlice)-1; i += 3 {
		colName := strQueryStringSlice[i]
		if colName == " " || isDQLCommandKeyword(colName) ||
			isPunctuationKeyword(colName) || isDataTypeKeyword(colName) {
			return fmt.Errorf(
				"expected a valid column-name but received '%s'", colName,
			)
		}
		colDataType := strQueryStringSlice[i+1]
		if !isDataTypeKeyword(colDataType) || isDQLCommandKeyword(colDataType) ||
			isPunctuationKeyword(colDataType) {
			return fmt.Errorf(
				"expected a valid column data-type but received '%s'", colDataType,
			)
		}
		commaOrEndParenthesisToken := strQueryStringSlice[i+2]
		if commaOrEndParenthesisToken != "," && commaOrEndParenthesisToken != ")" {
			return fmt.Errorf(
				"expected ',' but received '%s'", commaOrEndParenthesisToken,
			)
		}
	}
	return nil
}
