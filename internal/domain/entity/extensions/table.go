package extensions

import (
	"fmt"
	"github.com/Valentin-Kaiser/go-dbase/dbase"
	"strings"
)

const (
	COL_SPACE = 2
)

type Table struct {
	Table *dbase.File
}

func (t Table) Print() {
	if t.Table.EOF() {
		return
	}
	fmt.Print("|")
	fmt.Print("\t")
	for _, col := range t.Table.ColumnNames() {
		fmt.Printf("%s", col)
		fmt.Print("\t")
		fmt.Print("|")
		fmt.Print("\t")
	}
	fmt.Println()
	fmt.Print(strings.Repeat("_", 33))
	fmt.Println()
	for !t.Table.EOF() {
		row, err := t.Table.Next()
		if err != nil {
			panic(dbase.GetErrorTrace(err))
		}
		fmt.Print("|")
		fmt.Print("\t")
		for colIndex := range row.Fields() {
			fmt.Printf("%s", row.Field(colIndex).GetValue())
			fmt.Print("\t")
			fmt.Print("|")
			fmt.Print("\t")
		}
		fmt.Println()
	}

}
